package ru.vladbstrv;


import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        Main main = new Main();
        Method method = main.getClass().getMethod("myMethod");

        if (method.isAnnotationPresent(MyAnnotation.class)) {
            MyAnnotation annotation = method.getAnnotation(MyAnnotation.class);
            System.out.println(annotation.value());
        }

        main.myMethod();
    }

    @MyAnnotation
    public void myMethod() {
        System.out.println("Метод с аннотацией");
    }
}

